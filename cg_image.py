from PIL import Image
import math
from math import floor,ceil,pi,cos,sin
im = Image.open("Lenna.png")
px = im.load()

from numpy import linalg,ndarray

def resample(x,y,interp):
    if interp=="FORWARDS":
        return px[int(x),int(y)]
    if interp=="NN":
        try:
            return px[round(x),round(y)]
        except:
            return (0,255,0)
    if interp=="BILINEAR":
        #http://en.wikipedia.org/wiki/Bilinear_interpolation#Algorithm
        x1 = floor(x)
        x2 = ceil(x)
        y1 = floor(y)
        y2 = ceil(y)
        if x2==x1: x2+=1
        if y1==y2: y2+=1
        
        res = [0,0,0]
        try:
            #for each channel (RGB)
            for i in range(3):
                if x2>=im.size[0]: x2-=1
                if y2>=im.size[1]: y2-=1
                q11 = px[x1,y1][i]
                q21 = px[x2,y1][i]
                q12 = px[x1,y2][i]
                q22 = px[x2,y2][i]
                #x direction interp
                if x2==x1:
                    r1 = q11
                    r2 = q12
                else:
                    r1 = (((x2-x)/(x2-x1))*q11)+(((x-x1)/(x2-x1))*q21)
                    r2 = (((x2-x)/(x2-x1))*q12)+(((x-x1)/(x2-x1))*q22)
                #y direction interp
                if y1==y2:
                    res[i] = (r1+r2)/2
                else:
                    res[i] = ((y2-y)/(y2-y1))*r1
                    res[i]+= ((y-y1)/(y2-y1))*r2
                res[i] = int(round(res[i]))
            return tuple(res)
        except:
            print x1,x2,y1,y2,ousize,im.size
            raise
        

offset = (0,0)
transformMat = ndarray((3,3))
def reverseTransform(scale,a,x,y):

    vec = ndarray(3)
    vec[0] =x
    vec[1] =y
    vec[2] =1
    vecT = linalg.solve(transformMat,vec)
    return (vecT[0],vecT[1])
   


    #print (x,y),(xt,yt),res
    #return res

def transform(scale,angle,interp):
    global transformMat
    global offset
    global ousize
    global ou
    global opx
    
    

    print "size",
    print str(ousize)
    
    print "offset",
    print offset
    transformMat[0,0] = scale*cos(angle)
    transformMat[0,1] = sin(angle)
    transformMat[0,2] = offset[0]
    transformMat[1,0] = -1*sin(angle)
    transformMat[1,1] = scale*cos(angle)
    transformMat[1,2] = offset[1]
    transformMat[2,0] = 0
    transformMat[2,1] = 0
    transformMat[2,2] = 1
    
    totalPix = ou.size[1]*ou.size[0]
    tenperc = totalPix/10
    c = 0
    print "processing "+str(totalPix)+" pixels"
    for oy in range(ou.size[1]):
        for ox in range(ou.size[0]):
            x,y = reverseTransform(scale,angle,ox,oy)
            opx[ox,oy] = resample(x,y,interp)
            #out[ox,oy] = 
            #print ox,oy
            c+=1
            if c%tenperc==0:
                print str(c/tenperc)+"0%"
            
ou = 0
ousize = (0,0)
opx = 0
scale = 1;
out = {}
angle = 90
h , w = im.size
angle = math.radians(angle)
ht = h*cos(angle)+w*sin(angle)
wt = h*sin(angle)+w*cos(angle)


transformMat[0,0] = scale*cos(angle)
transformMat[0,1] = sin(angle)
transformMat[0,2] = offset[0]
transformMat[1,0] = -1*sin(angle)
transformMat[1,1] = scale*cos(angle)
transformMat[1,2] = offset[1]
transformMat[2,0] = 0
transformMat[2,1] = 0
transformMat[2,2] = 1
a = (0,0)
b = (im.size[0]-1,0)
c = (0,im.size[1]-1)
d = (b[0],c[1])
# a b
# ---
# | |
# ---
# c d

at = reverseTransform(scale,angle,a[0],a[1])
bt = reverseTransform(scale,angle,b[0],b[1])
ct = reverseTransform(scale,angle,c[0],c[1])
dt = reverseTransform(scale,angle,d[0],d[1])
print "corners"
print at,bt,ct,dt
smallestX = min(at[0],bt[0],ct[0],dt[0])
biggestX = max(at[0],bt[0],ct[0],dt[0])
smallestY = min(at[1],bt[1],ct[1],dt[1])
biggestY = max(at[1],bt[1],ct[1],dt[1])
ousize = (int(biggestX-smallestX),int(biggestY-smallestY))

offset = (0,bt[1])
ou = Image.new(im.mode,ousize)


opx = ou.load()
transform(scale,angle,"NN")
ou.save("test.png")

##
##for i,j in out:
##    #print i,j,out[i,j]
##    opx[i,j] = out[i,j]

##print "done"
